# TwitterAPI - FB OversightBoard 

Etienne P Jacquot - 04/13/2021

## Getting Started

This project uses Docker & Python to execute TwitterAPI calls. 

- *For the previous README with detailed notes & instructions on reproducing & troubleshooting, please visit [archive/readme.md](./archive/README.md).*

______

### Twitter API Access

Please navigate to the Twitter developer portal [https://developer.twitter.com](https://developer.twitter.com) to get your API keys. 

- Once you have your access tokens, please save them as `/config/config.ini`
    - Please note, I added `*.ini` to the [.gitignore](.gitignore) to prevent my tokens from syncing to the repo
    - Format for the file:

#### *We are running two different API calls for this project*

- One for **StreamListener** (data already on AWS) for *engagement* with the twitter accounts
- One for **TimelineGetter** (*data not yet on aws*) for all tweets from an account (up to 3,200 tweets)

The first ran continously until 12/08/2021 when the server crashed unfortunately. The latter I tried running once a day, but this was redudant given neither account has greater than 3,200 tweets. This *might* have captured some tweet IDs that no longer exist (are deleted) so worth exploring. For now I am just going to pull timelines for our two accounts.

________


### Deploying on NXITDockerSrv1 / Local Docker


#### Running Docker: 

- Confirm your local host has docker installed, more info [here](https://www.docker.com/get-started). 

Our two twitter accounts of interest: 

1. `@fboversight`
2. `@oversightboard`

#### _STREAM-LISTENER_:

- Get your json outfiles from NXDockerSrv1 for the stream listeners (again this ran in the past ending on 12/08/2021), then run the [etl-simple.ipynb](./notebooks/etl-simple.ipynb) notebook for cleanup & upload to AWS.

#### _TIMELINE-GETTEER_:

- Run code in new notebook TBD....
- Is there daily run data available from the daily cronjob run? Not sure need to review

__________

### Directories:

[archive](./archive):
- Archived work & sample files for reference

[configs](./configs)
- pip requirement text file, `config.ini` that you must create w/ Twitter API tokens, and cronjob examples

[data](./data) (Directory to mount to docker container for output files): 
- [stream_oversightboard](./data/stream_oversightboard') for stream results
- [timeline_oversightboard](./data/timeline_oversightboard') contains two directories for the daily job (INGORE) 

[scripts](./scripts)
- Example Python scripts for stream & timeline, contingent on Twitter username passed as argument in Dockerfile

[notebooks](./notebooks) <-- **_NEW DIRECTORY 04/13/2021_**
- Example notebooks for ETL of StreamListener JSON results, and TimelineGetter deploymenet for most recent 3,200 tweets


[timeline](./timeline) <-- **_OLD! Not really ever used_** 
- [oversightboard](./timeline/timeline_oversightboard) is main directory with script, has a [dockerfile](./timeline/dockerfile). You *must edit for your specific twitter username of interest!*...

[bots](./bots): <-- **_FOR CONTINUOUS DOCKER DEPLOYMENT_**
- [oversightboard](./bots/stream_oversightboard/) is main directory w/ [Dockerfile](./bots/dockerfile) which configures the stream listener, edit for specific username
  - Please note there are various other bots listed, this was for testing...
_______
