# LEGACY README WITH OLD NOTES & INSTRUCTIONS!

# Testing Docker Python app for Tweepy Twitter API

Etienne Jacquot - 02/15/2021

## *Revisiting this project with the etl-simple notebook* 

--> [etl-simple.ipynb](./etl-simple.ipynb) takes twitter api stream json file and exports to parquet... this reveals 8,815 tweets captures, some retweets and others tweets at the account. This is both `fboversight` and `oversightboard` ...

--> **TODO:** I have not reviewed the *timeline* data, this likely did *not* include pagination... You can get up to 3,200~ tweets for a timeline via TwitterAPIv2, I had this as a daily cronjob which also failed on 12/08...


--> The **NXITDockerSrv** was a great learning experiencing in trying to host containerized Twitter API instances... 
The server's root partition filled up on 12/08/2020 which caused all processes to crash, I didn't notice until 02/2021 which is frustrating given DJT was banned from Twitter & Facebook in early January 2021... 
- These workloads are maybe best run in the cloud??

____________

## Merging strategies for branching...

I have some old branches, specifically *nxitdockersrv* which I just deleted for the sake of clarity. I added various other tweepy-bots for that account given it was already relatively setup on the server (I have since removed Diami's project and created a respective repo)...

- I need to review the other branches for anything significant...

--> Thinking about simplifying the twitterAPI streamListeners!

## Reviewing the two json files

I am surprised at the filesize... it's barely 60MB... 

interestingly one line was so long it just had `...` at the end... preventing me from pd.read_json(lines=True) I had to find the specific line which was breaking this...
- is that a limitation of visual studio code json viewer? after removing that line I was able to read into jupyter python notebook without the exception...

_____________

## Etienne Jacquot - 07/10/2020

### Getting Started:

- Start by cloning this repository to your localhost 
        - If the below fails, you'll need to request permission for this private repo...

`git clone git@bitbucket.org:annenbergschool/twitterapi-oversightboard.git`

- Make sure to mount the JANUS\ITDevOps H drive to the host

```
dnf install samba-client samba-common cifs-utils

mkdir /mnt/itdevops_hdrive

# add the following line to /etc/fstab
# manually enter credentials... 
# How can we harden this by having /etc/fstab read from a config file?

\\nxfilesrv1.asc.upenn.edu\HomeStaff\itdevops           /mnt/itdevops_hdrive    cifs    user,uid=1000,rw,suid,username=itdevops@asc.upenn.edu,password=******** 0 0

# To confirm the drive is mounted
mount -a

# Confirm SMB mount for H drive is working as expected 

ls -l /mnt/itdevops/BitBucket
``` 

### Running Docker: 

- Confirm your local host has docker installed, more info [here](https://www.docker.com/get-started)

### Twitter Access Tokens:

- Before you proceed, please make sure you have Twitter access tokens readily available. If not, please visit [https://developer.twitter.com](https://developer.twitter.com) to create a developer account, an app, wait for app approval, and then generate your consumer & access tokens
	- For more information, please visit [here](https://developer.twitter.com/en/docs/basics/authentication/oauth-1-0a/obtaining-user-access-tokens)

- Once you have your access tokens, please save them as `/config/config.ini`
    - Please note, I added `*.ini` to the [.gitignore](.gitignore) to prevent my tokens from syncing to the repo
    - Format for the file:

```
[TWITTER]
consumer_key = [API-key]
consumer_secret = [API-secret-key]
access_key = [Access-token]
access_secret = [Access-token-secret]
```
_______________


### Directories:

- [bots](./bots)
    - [oversightboard](./bots/stream_oversightboard/) is main directory, script should be relatively automated given the Dockerfile specifies the Twitter input name
    - Has a [Dockerfile](./bots/dockerfile) which configures the stream listener, edit for specific username

- [timeline](./timeline)
    - [oversightboard](./timeline/timeline_oversightboard) is main directory with script, this should also be automated given Dockerfile has Twitter username of interest...
    - Has a [dockerfile](./timeline/dockerfile) that you **must edit for your specific twitter username of interest!**...

- [data](./data)
    - Directory to mount to docker container for output files... 
    - [stream_oversightboard](./data/stream_oversightboard') 
	- This is just a continuously written file in this directory
    - [timeline_oversightboard](./data/timeline_oversightboard') contains two directories for the dialy job...
    	- [raw](./data/stream_oversightboard/raw/) for json dumps of timeline so we have raw data
	- [etl](./data/stream_oversightboard/elt/) for parquet file which aims to replicate Wharton twitter historical db on AWS Athena

- [configs](./configs)
    - pip requirement text file
    - config.ini that you need to create with your Twitter access tokens...
    - cronjob example text which you should add to your host with `cronjob -e` and `systemctl status crond` to check that it's running
    
- [scripts](./scripts)
    - example scripts for bot & timeline containers ... Should be dependent on Dockerfile having an argument which passes Twitter Username... 
_______

## Running Stream Listener for OversightBoard tweets: 

- This containers dockerfile is located at [bots/dockerfile]

``` bash

cd /this/repo

# Review your dockerfile to confirm it's running correct bot for twitter username

cat bots/dockerfile

# check your app auth configs
cat configs/config.ini
# and make sure that your respective py scripts read the correct confg ini set:
cat bots/<twitter_username>/tweepy_stream.py | grep config

# To Docker build your container image run the following
# Running on CentOS8 host NXITDockerSrv1 needs explicit network flag

docker build --network=host -t oversightboard-tweepy-listener -f bots/dockerfile .

# To launch your tweepy listener container from your image
# Set your bind mount to a directory in the repo

docker run -it
	--network=host
	-v /mnt/itdevops_hdrive/BitBucket/twitterapi-oversightboard/data:/data
	--restart always
	oversightboard-tweepy-listener

# To check and confirm tweepy listeners are working

[ascroot@CentOS8 twitterapi-oversightboard]$ docker ps
CONTAINER ID        IMAGE                                    COMMAND                  CREATED             STATUS              PORTS               NAMES
7cf19b781cfa        undersightboard-tweepy-listener:latest   "python3 bots/unders…"   24 hours ago        Up 4 minutes                            hopeful_williams
8183520e9f18        oversightboard-tweepy-listener           "python3 bots/oversi…"   26 hours ago        Up 26 hours                             boring_robinson

# If a container goes down (it eventually will!) due to network drop or unknown error, attach the container to see stream output

[ascroot@CentOS8 twitterapi-oversightboard]$ docker attach boring_robinson 
@OversightBoard i thought you were all about free speech, Mark https://t.co/menobDFojF
@OversightBoard so not until the election could be ruined by Russian interlocutors? Good job doing nothing, shitlords.
@OversightBoard
RT @OversightBoard: We understand many people are eager for the Board to officially begin our task of providing independent oversight of Fa…

# The output file is json dumped

# New outdir:

ls -l data/stream_oversightboard 

# Old dir
[ascroot@CentOS8 twitterapi-oversightboard]$ ls -l data/ | grep OversightBoard
-rwxr-xr-x. 1 ascroot root 36056 Jul  9 23:45 OversightBoard_tweets.json

```
- **NOTES:** how to get the json dumped file and then perform ETL ... the stream does this, but the listener has ETL in the script ... All this suggests towards a workflow where raw data is just saved continuously / daily, and then other processes grab that data!
_________

## Running Daily OversightBoard Timeline Fetcher

- This containers dockerfile is located in `timeline/dockerfile`

- Helpful to schedule this docker run job with system `cronjob` as a daily task... More on this later. Helpful site for calculating your cronjob job syntax [here](https//crontab.guru)

- Testing JSON dump for timeline getter, and also including ETL outdir
	- Realizing this script needs to dump json, and then separately perform ETL I guess with another container / script? ... 
	-**OLD**: The python script basically gets a users timeline up to 200 tweets, then extracts from `status._json` all the relevant columns / datapoints of interest. I decided to replicate based on the Twitter archive on Wharton's AWS instance, I don't have all columns replicated but for I do for the most part ... 

```
cd /this/repo

# Build your container which will get Twitter users Timeline 

# create raw & etl outdirs (really this should be in the script)
mkdir ./data/<twitter_username>
mkdir ./data/<twitter_username>/raw 
mkdir ./data/<twitteR_username>/etl

# Build your container which will get Twitter users timeline going back 200 tweets

docker build --network=host -t timeline_oversightboard -f timeline/dockerfile .

# This will mount the repo directory ./data/timeline_oversightboard -> /data on the container
# If you are running for a different twitter user, make sure to change the directory mount!

docker run -it 
	--network=host
	-v /mnt/itdevops_hdrive/BitBucket/twitterapi-oversightboard/data:/data timeline_oversightboard

# Confirm the timeline getter extracted their twitter feed as expected:

ls -l data/timeline_oversightboard

```

### Automating your timeline getter container with `crontab`

- Run the following lines to set this up

```
# Check if crond is setup:

systemctl status crond.service

# View example crontab config, helpful to copy this to clipboard

cat configs/crontab_example

# To set a crontab for your Docker run timeline getter:

crontab -e 

# Paste the crontab example into your crontab file and save! add the following to schedule at 1:30AM

##############################
#
# ASC NXITDockerSrv1 Crontab
#
# Etienne Jacquot - 07/10/2020
#
# COPY CONTENTS OF THIS INTO YOUR CRONTAB!
#
##############################
#
# Daily run to get OversightBoard Twitter timeline
#
05 0 * * * /usr/bin/docker run -d --network=host -v /mnt/itdevops_hdrive/BitBucket/twitterapi-oversightboard/data/:/data timeline_oversightboard:latest
#
##############################

# Notice the differences in this crontab config and the manual docker run ... 

# This example sets crontab to automatically launch that container at 12:05AM daily!

```

- **Notes**: 
    - Working to setup docker on the CentOS8 host NXITDockerSrv1, for the most past this is up and running ... Just want to confirm with the team how to arrange the bots & timeline directories so it makes sense... Also the scripts contain hardcoded values for Twitter usernames so need to update this as well, possibly taking sys.argv[0] input in dockerfile CMD ... 
    - permissions on outfiles, I think this relates to docker in wheel group ... https://vsupalov.com/docker-shared-permissions/ 
    - Do we want the full json object returned by Tweepy, or just export the json as I have ... or both? Currently the bots get a full json, but the timeline gets an ETL output version of this ... 

________

### Helpful Commands for Managing Docker Containers

```
# To check status of running containers

docker ps

# Example output:
[ejacquot@asc.upenn.edu@dockersrv1 ~]$ docker ps
CONTAINER ID        IMAGE                            COMMAND                  CREATED             STATUS              PORTS               NAMES
35563c039a45        tweepy-ppd-listener              "python3 bots/tweepy…"   3 hours ago         Up 3 hours                              stoic_borg
1221a2554b33        oversightboard_tweepy_listener   "python3 bots/tweepy…"   3 hours ago         Up 3 hours                              clever_franklin

<<<<<<< HEAD
### Directories:

- [bots](./bots)
	- Contains directories for twitter usernames and python script for listener
- [configs](./configs)
	- pip requirement text file
	- config.ini file that you must create with your Twitter access tokens...
- [data](./data)
	- Mount for streamListener is this directory
	
### Furher testing ... 

- @OversightBoard, I think this is the retweet stream. Saving output as json
- Listening for DJT @realDonaldTrump testing as there are many retweeets
- Now listening to PPD to pass results to rekognition
_______

### I keep occasionally getting this error :
=======
# If you want to quickly stop & delete a container

docker stop CONTAINER_ID ; docker rm CONTAINER ID

```

#### OversightBoard has been occassionally failing ... set container to always restart!

- Not yet sure why --  mentions the following, it certainly has something to do with occasional network drops ... The question is, using Docker how can we have this so that if the process ever fails, another container spins right up and reopens the json file to continue writing...:

>>>>>>> a72032598f229ba6243a8709f41d1a9c95369b29

``` python
RT @OversightBoard: How Facebook treats posts from public figures                       that may violate the community standards are within the scope of t                      he Boar…
Traceback (most recent call last):
  File "/usr/local/lib/python3.7/http/client.py", line 554, in _get_chunk_left
    chunk_left = self._read_next_chunk_size()
  File "/usr/local/lib/python3.7/http/client.py", line 521, in _read_next_chunk_size
    return int(line, 16)
ValueError: invalid literal for int() with base 16: b''

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "/usr/local/lib/python3.7/http/client.py", line 586, in _readinto_chunked
    chunk_left = self._get_chunk_left()
  File "/usr/local/lib/python3.7/http/client.py", line 556, in _get_chunk_left
    raise IncompleteRead(b'')
http.client.IncompleteRead: IncompleteRead(0 bytes read)

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "/usr/local/lib/python3.7/site-packages/urllib3/response.py", line 437, in _error_catcher
    yield
  File "/usr/local/lib/python3.7/site-packages/urllib3/response.py", line 519, in read
    data = self._fp.read(amt) if not fp_closed else b""
  File "/usr/local/lib/python3.7/http/client.py", line 457, in read
    n = self.readinto(b)
  File "/usr/local/lib/python3.7/http/client.py", line 491, in readinto
    return self._readinto_chunked(b)
  File "/usr/local/lib/python3.7/http/client.py", line 602, in _readinto_chunked
    raise IncompleteRead(bytes(b[0:total_bytes]))
http.client.IncompleteRead: IncompleteRead(464 bytes read)

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "bots/tweepy_oversightboard.py", line 43, in <module>
    stream.filter(follow=[str(api.get_user('OversightBoard').id)])
  File "/usr/local/lib/python3.7/site-packages/tweepy/streaming.py", line 474, in filter
    self._start(is_async)
  File "/usr/local/lib/python3.7/site-packages/tweepy/streaming.py", line 389, in _start
    self._run()
  File "/usr/local/lib/python3.7/site-packages/tweepy/streaming.py", line 320, in _run
    six.reraise(*exc_info)
  File "/usr/local/lib/python3.7/site-packages/six.py", line 703, in reraise
    raise value
  File "/usr/local/lib/python3.7/site-packages/tweepy/streaming.py", line 289, in _run
    self._read_loop(resp)
  File "/usr/local/lib/python3.7/site-packages/tweepy/streaming.py", line 339, in _read_loop
    line = buf.read_line()
  File "/usr/local/lib/python3.7/site-packages/tweepy/streaming.py", line 200, in read_line
    self._buffer += self._stream.read(self._chunk_size)
  File "/usr/local/lib/python3.7/site-packages/urllib3/response.py", line 541, in read
    raise IncompleteRead(self._fp_bytes_read, self.length_remaining)
  File "/usr/local/lib/python3.7/contextlib.py", line 130, in __exit__
    self.gen.throw(type, value, traceback)
  File "/usr/local/lib/python3.7/site-packages/urllib3/response.py", line 455, in _error_catcher
    raise ProtocolError("Connection broken: %r" % e, e)
urllib3.exceptions.ProtocolError: ('Connection broken: IncompleteRead(464 bytes read)', IncompleteRead(464 bytes read))
```
_________
### Troubleshooting

- Originally looked at tweepy documentation & jhub notebooks from Mbod's Comm313 class ... 

- Tested with outputing to json file for live stream, this works well but no one is really interacting with @OversightBoard ... trying to build a test tweepy-bot to listen for DJT tweets and in real time writes to json ... 

- Tried testing on python3.7-alpine but apparently that does not work well with pip install pandas, online I found recommendation suggesting use `python-slim` instead, more info [here](https://github.com/docker-library/python/issues/381)
