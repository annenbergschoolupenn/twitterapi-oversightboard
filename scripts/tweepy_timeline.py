import tweepy
import configparser
import os
import json
import sys
import datetime
from time import strptime
import pandas as pd
import pyarrow
#from collections import Counter

# Input from Dockerfile for Username
username = sys.argv[1]

# You must create a Twitter app & get tokens for this step!
# Upload your twitter access tokens to configs/config.ini file!
twitter_cred = {}
config = configparser.ConfigParser()
config.read('configs/config.ini') # <--- add your Twitter API tokens to this file!
for item,value in config['TWITTER'].items():
    twitter_cred[item]=value

# Setup Twitter Authentication
auth = tweepy.OAuthHandler(twitter_cred['consumer_key'], twitter_cred['consumer_secret'])
auth.set_access_token(twitter_cred['access_key'], twitter_cred['access_secret'])

# Create Twitter API object
api = tweepy.API(auth, 
                 wait_on_rate_limit=True, 
                 wait_on_rate_limit_notify=True)
                 #,parser=tweepy.parsers.JSONParser())

# Test API authentication
try:
    api.verify_credentials()
    print("Authentication OK!")
except:
    print("Error during authentication...")
    
# Get 200 tweets from OversightBoard twitter account
# This account only has 33 tweets so far so not a concern, but there are pagination limits ... 
twitter_timeline = api.user_timeline(user_id=api.get_user(username).id, count=200)

# need to json dump api.user_timeline that is returned ... 
#twitter_timeline 

########################################################################################
# Preparing to get twitter content of interest into pandas DF

tweets_list_of_dicts = []

for status in twitter_timeline:
    
    # each row in pandas df is one tweet
    tweet_dict = {}
    for key in status._json.keys():

        # the following columns map cleanly to results returned by twitter API
        tweet_dict["created_at"] = status._json["created_at"]
        tweet_dict["id"] = status._json["id"]
        tweet_dict["id_str"] = status._json["id_str"]
        tweet_dict["source"] = status._json["source"]
        tweet_dict["truncated"] = status._json["truncated"]
        tweet_dict["in_reply_to_status_id"] = status._json["in_reply_to_status_id"]
        tweet_dict["in_reply_to_status_id_str"] = status._json["in_reply_to_status_id_str"]
        tweet_dict["in_reply_to_user_id_str"] = status._json["in_reply_to_user_id_str"]
        tweet_dict["in_reply_to_screen_name"] = status._json["in_reply_to_screen_name"]
        tweet_dict["contributors"] = status._json["contributors"]
        tweet_dict["is_quote_status"] = status._json["is_quote_status"]
        tweet_dict["retweet_count"] = status._json["retweet_count"]
        tweet_dict["favorite_count"] = status._json["favorite_count"]
        tweet_dict["favorited"] = status._json["favorited"]
        tweet_dict["retweeted"] = status._json["retweeted"]
        tweet_dict["lang"] = status._json["lang"]
        tweet_dict["text"] = status._json["text"]

        # Adding date columns for SQL querying date ranges
        tweet_dict["year"] = int(status._json["created_at"].split()[-1])
        tweet_dict["month"] = strptime('{}'.format(status._json["created_at"].split()[1]),'%b').tm_mon #status._json["month"]
        tweet_dict["day"] = int(status._json["created_at"].split()[2])
        tweet_dict["hour"] = int(status._json["created_at"].split()[3].split(":")[0])
        
        # Other columns map less cleanly...
        # Will need to investigate how to get these columns, or ask Wharton ... 
        # It's my suspicious these all should be in their own try catch 

        tweet_dict["extended_full_text"] = [] #status._json["extended_full_text"]
        tweet_dict["timestamp_ms"] = [] #status._json["timestamp_ms"]
        tweet_dict["quote_count"] = [] #status._json["quote_count"]
        tweet_dict["reply_count"] = [] #status._json["reply_count"]
        tweet_dict["filter_level"] = [] #status._json["filter_level"]
        tweet_dict["possibly_sensitive"] = [] #status._json["possibly_sensitive"] # <--- Not sure about this one yet!
        tweet_dict["withheld_copyright"] = [] #status._json["withheld_copyright"]
        tweet_dict["place_name"] = [] #status._json["place_name"]
        tweet_dict["quoted_status_id_str"] = [] #status._json["quoted_status_id_str"]
        tweet_dict["place_country_code"] = [] #status._json["place_country_code"]
    
        # For sake of catching errors, I have it loop through keys ... 
        # this is probably not the most efficient, but helpful in testing
        
        if key == 'lat':
            tweet_dict["lat"] = status._json["lat"]

        if key == 'long':
            tweet_dict["long"] = status._json["long"]
            
        # Getting User details which is nested in the response
        if key == 'user':
            tweet_dict['user_id'] = status._json['user']['id']
            tweet_dict['user_screen_name'] = status._json['user']['screen_name']
            tweet_dict['user_name'] = status._json['user']['name']
            tweet_dict['user_followers_count'] = status._json['user']['followers_count']
            tweet_dict['user_location'] = status._json['user']['location']
            tweet_dict['user_created_at'] = status._json['user']['created_at']
            tweet_dict['user_friends_count'] = status._json['user']['friends_count']
            tweet_dict['user_statuses_count'] = status._json['user']['statuses_count']
            tweet_dict['user_verified'] = status._json['user']['verified']
            
            
        # Get RT details if tweet is a retweet!
        # Get Retweet Details if tweet is a retweet
        if key == "retweeted_status":
            tweet_dict['rt_id'] = status._json['retweeted_status']['id']
            tweet_dict['rt_user_name'] = status._json['retweeted_status']['user']['name']
            tweet_dict['rt_extended_full_text'] = [] #status._json['retweeted_status']['xxx'] <--- NOT SURE ABOUT THIS ONE?
            tweet_dict['rt_favorite_count'] = status._json['retweeted_status']['favorite_count']
            tweet_dict['rt_friends_count'] = status._json['retweeted_status']['user']['friends_count']
            tweet_dict['rt_followers_count'] = status._json['retweeted_status']['user']['followers_count']
            tweet_dict['rt_statuses_count'] = status._json['retweeted_status']['user']['statuses_count']
            tweet_dict['rt_user_id'] = status._json['retweeted_status']['user']['id']
            tweet_dict['rt_retweet_count'] = status._json['retweeted_status']['retweet_count']
            tweet_dict['rt_reply_count'] = [] # status._json['retweeted_status']['xxx'] <--- NOT SURE ABOUT THIS ONE?
            tweet_dict['rt_quote_count'] = [] # status._json['retweeted_status']['xxx'] <--- NOT SURE ABOUT THIS ONE?
            tweet_dict['rt_user_verified'] = status._json['retweeted_status']['user']['verified']
            tweet_dict['rt_truncated'] = status._json['retweeted_status']['truncated']
            tweet_dict['rt_source'] = status._json['retweeted_status']['source']
            tweet_dict['rt_created_at'] = status._json['retweeted_status']['created_at']
            tweet_dict['rt_user_screen_name'] = status._json['retweeted_status']['user']['screen_name']
            tweet_dict['rt_text'] = status._json['retweeted_status']['text']
            tweet_dict['rt_user_location'] = status._json['retweeted_status']['user']['location']

    # Finally once the tweet status._json object info has been extracted
    # Setup as list of dicts for a pandas Dataframe
    tweets_list_of_dicts.append(tweet_dict)    


twitter_df = pd.DataFrame(tweets_list_of_dicts)
print("Returned {} tweets for username {}".format(twitter_df.shape[0],username))
outdir = '/data/timeline_{}'.format(username) # <--- Remember, this is outdir INSIDE the docker container... Docker Volume --mount flag for /data/twitter/ so you get the output file!
filename = username+"_"+str(datetime.date.today())+".parquet"
filename_raw = username+"_"+str(datetime.date.today())+"_dump.json"

# Check for outdir
if not os.path.exists(outdir):
    os.makedirs(outdir)

# Exporting raw Twitter timeline as pandas dataframe of statuses
twitter_raw_df = pd.DataFrame()
for status in twitter_timeline:
    twitter_raw_df = twitter_raw_df.append(status._json,ignore_index=True)
twitter_raw_df.to_json("{}/raw/{}".format(outdir,filename_raw))

# Exporting ETL to parquet file for query in Athena
twitter_df.to_parquet("{}/etl/{}".format(outdir,filename))
print('Complete!')
