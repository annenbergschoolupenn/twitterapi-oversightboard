#############################################
## Etienne Jacquot
## 06/16/2020
## 
## Testing Tweepy for Twitter API streaming 

import tweepy
import configparser
import os
import json
#from collections import Counter

# Get twitter access tokens from config.ini file (you need to create a Twitter app & get tokens for this step!)
twitter_cred = {}
config = configparser.ConfigParser()
config.read('configs/config.ini') # <--- add your Twitter API tokens to this file!
for item,value in config['TWITTER'].items():
    twitter_cred[item]=value

# Create Twitter auth API object 
auth = tweepy.OAuthHandler(twitter_cred['consumer_key'], twitter_cred['consumer_secret'])
auth.set_access_token(twitter_cred['access_key'], twitter_cred['access_secret'])
#api = tweepy.API(auth)
api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

# Create StreamListener Class
class StreamListener(tweepy.StreamListener):

    def on_status(self, status):
        with open('/data/{}'.format(self.filename),'a') as out: 
            out.write(json.dumps(status._json)+'\n') # <--- writes TwitterStream results to json file
        print(status.text)

    def on_error(self, status_code):
        if status_code == 420:
            print('Access error - wait to try again')
            return False
        
# Listen to TwitterAPI Stream
stream_listener = StreamListener()
stream_listener.filename = 'UndersightBoard_tweets.json'
stream = tweepy.Stream(auth=api.auth, listener=stream_listener)
stream.filter(follow=[str(api.get_user('UndersightBoard').id)])
