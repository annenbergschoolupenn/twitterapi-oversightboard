#############################################
## Etienne Jacquot
## 07/10/2020
## Tweepy Script for Twitter API streaming 
#############################################

import tweepy
import configparser
import os
import json
import sys

# Get twitter access tokens from config.ini file
twitter_cred = {}
config = configparser.ConfigParser()
config.read('configs/config_fboversight.ini') # <--- add your Twitter API tokens to this file!
for item,value in config['TWITTER'].items():
    twitter_cred[item]=value

# Create Twitter auth API object 
auth = tweepy.OAuthHandler(twitter_cred['consumer_key'], twitter_cred['consumer_secret'])
auth.set_access_token(twitter_cred['access_key'], twitter_cred['access_secret'])
#api = tweepy.API(auth)
api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

# Create StreamListener Class
class StreamListener(tweepy.StreamListener):

    def on_status(self, status):
        with open('/data/stream_{}/{}'.format(username,self.filename),'a') as out: # <--- Docker bind mount :/data
            out.write(json.dumps(status._json)+'\n') # <--- writes TwitterStream to json file
        print(status.text)

    def on_error(self, status_code):
        if status_code == 420:
            print('Access error - wait to try again')
            return False
        
# Input from Dockerfile as Twitter username ...
username = str(sys.argv[1])
print('Tweepy Stream Listener for Twitter username: {}'.format(username))
print('-'*55)

# Check to make sure outdir existsi
# This is bindd mounted for snapstream
if not os.path.exists('/data/stream_{}'.format(username)):
    os.makedirs('/data/stream_{}'.format(username))


# Listen to TwitterAPI Stream
# I think filename need to be lowercase...?
stream_listener = StreamListener()
stream_listener.filename = '{}_stream.json'.format(username)
stream = tweepy.Stream(auth=api.auth, listener=stream_listener)

# Adding stall warning true
stream.filter(follow=[str(api.get_user('{}'.format(username)).id)],stall_warnings=True)
